public class MipsDisassembler {


    // Bit Masks
    private static final int maskOpCode = 0xFC000000;       // 111111-00000-00000-00000-00000-000000
    private static final int maskSrc1 = 0x03E00000;         // 000000-11111-00000-00000-00000-000000
    private static final int maskSrc2Dst = 0x001F0000;      // 000000-00000-11111-00000-00000-000000
    private static final int maskDst = 0x0000F800;          // 000000-00000-00000-11111-00000-000000
    private static final int maskXBits = 0x000007C0;        // 000000-00000-00000-00000-11111-000000
    private static final int maskFunc = 0x0000003F;         // 000000-00000-00000-00000-00000-111111
    private static final int maskOffset = 0x0000FFFF;       // 000000-00000-00000-1111111111111111


    // MIPS Fields
    private static int opCode = 0;
    private static int src1 = 0;
    private static int src2Dst = 0;
    private static int dst = 0;
    private static int xBits = 0;
    private static int func = 0;
    private static short offset = 4;
    private static String opCodeWord = "";


    // Starting Address
    private static int address = 0x9A040;




    /**
     * This method is a container method in which an integer array of 32-bit hex memory addresses are disassembled into MIPS instructions
     * @param instructions This is an array of 32-bit integers. It is the baseline requisite input for the disassembler to work.
     */
    public MipsDisassembler(int[] instructions) throws Exception {

        for(int eachHexInstruction: instructions){
            /*
            Determine if first 6 bits equal zero. If so, then we know MIPS instruction is R-Format. Otherwise, instruction will be interpreted as I-Format.
             */
            startDisassembly(eachHexInstruction);
            incrementAddress();
        }
    }



    /**
     * This method builds an integer array of all R-Formatted fields for MIPS instructions
     * @param mipsInstruction This is the 32-bit integer that will be analyzed
     */
    private void disassembeRformatFields(int mipsInstruction) throws Exception {

        // Error handling
        if(opCode != 0)
            throw new Exception("I-Format Instruction Detected in R-Format Disassembler");

        // Calculate R-Format fields
        src1 = getSrc1(mipsInstruction);
        src2Dst = getSrc2Dst(mipsInstruction);
        dst = getDst(mipsInstruction);
        xBits = getXBits(mipsInstruction);
        func = getFunc(mipsInstruction);

        String funcWord = getFuncWord(func); // Derive word value (e.g. lw, sw) from function field

        // Print R-Format Instruction
        System.out.printf("%X ", address);
        System.out.println(funcWord + " $" + dst + " $" + src1 + " $" + src2Dst);
    }



    /**
     * This method builds an integer array of all I-Formatted fields for MIPS instructions
     * @param mipsInstruction This is the 32-bit integer that will be analyzed
     * @return An integer array of R-Formatted fields for MIPS instructions
     */
    private void disassembleIformatFields(int mipsInstruction) throws Exception {

        // Error handling
        if(opCode == 0)
            throw new Exception("R-Format Instruction Detected in I-Format Disassembler");

        // Calculate I-Format specific fields
        src1 = getSrc1(mipsInstruction);
        src2Dst = getSrc2Dst(mipsInstruction);
        offset = getOffset(mipsInstruction);

        if(opCode == 4 || opCode == 5)
            handleBranching(mipsInstruction);

        else {
            // I-Format Instruction
            System.out.printf("%X ", address);
            System.out.println(opCodeWord + " $" + src2Dst + ", " + offset + "($" + src1 + ")");
        }
    }


    /**
     * This method uses a bitwise mask to match and return the Op Code field from a MIPS instruction
     * @param hexInstruction This is the 32-bit MIPS instruction that will be referenced
     * @return A 32-bit integer containing only the matched Op Code bits; all non-matching bits are set to zero
     */
    private void startDisassembly(int hexInstruction) throws Exception {

        // First, calculate op code to determine instruction format
        opCode = getOpCode(hexInstruction);

        if( opCode == 4)
            opCodeWord = "beq";

        if( opCode == 5)
            opCodeWord = "bne";

        if( opCode == 35)
            opCodeWord = "lw";

        if( opCode == 43)
            opCodeWord = "sw";

        if(opCode == 0)
            disassembeRformatFields(hexInstruction);

        else
            disassembleIformatFields(hexInstruction);
    }



    /**
     * This method increments the MIPS memory address by 4-bytes.
     */
    private void incrementAddress(){
        address +=4;
    }



    /**
     * This method simply calculates branch-specific fields and prints them in proper branch format
     * @param instruction This is the reference MIPS instruction from which the branch fields will be calculated
     */
    private void handleBranching(int instruction){

        // Calculate branch destination label address
        int label = address + (offset << 2) + 4;

        // Print branch instruction
        System.out.printf("%X ", address);
        System.out.print(opCodeWord + " $" + src2Dst + ", $" + src1 + " ");
        System.out.printf("%X\n", label);
    }


    /**
     * This method uses a bit mask to match the OP Code field of a MIPS instruction
     * @param hexInstruction This is the reference MIPS instructions of which we will derive a OP Code value
     * @return Returns the OP Code field extracted from the reference MIPS instruction
     */
    private int getOpCode(int hexInstruction){

        int opCode = (hexInstruction & maskOpCode) >>> 26;

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskOpCode);
        Console.debugf("OPCODE: 0x%X", opCode);
        Console.debugf("","\n");

        return opCode;
    }



    /**
     * This method uses a bit mask to match the src1 field of a MIPS instruction
     * @param hexInstruction This is the reference MIPS instructions of which we will derive a src1 value
     * @return Returns the src1 field extracted from the reference MIPS instruction
     */
    private int getSrc1(int hexInstruction){

        int src1 = (hexInstruction & maskSrc1) >> 21;

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskSrc1);
        Console.debugf("SRC1: 0x%X", src1);
        Console.debugf("","\n");

        return src1;
    }



    /**
     * This method uses a bit mask to match the src2/dst field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a src2/dst value
     * @return Returns the src2/dst field extracted from the reference MIPS instruction
     */
    private int getSrc2Dst(int hexInstruction){

        int src2Dst = (hexInstruction & maskSrc2Dst) >> 16;

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskSrc2Dst);
        Console.debugf("SRC2DST: 0x%08X", src2Dst);
        Console.debugf("","\n");

        return src2Dst;
    }



    /**
     * This method uses a bit mask to match the dst field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a dst value
     * @return Returns the dst field extracted from the reference MIPS instruction
     */
    private int getDst(int hexInstruction){

        int dst = (hexInstruction & maskDst) >> 11;

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskDst);
        Console.debugf("DST: 0x%08X", dst);
        Console.debugf("","\n");

        return dst;
    }



    /**
     * This method uses a bit mask to match the X-Bits field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a X-Bits value
     * @return Returns the X-Bits field extracted from the reference MIPS instruction
     */
    private int getXBits(int hexInstruction){

        int xBits = (hexInstruction & maskXBits);

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskXBits);
        Console.debugf("XBITS: 0x%08X", xBits);
        Console.debugf("","\n");

        return xBits;
    }



    /**
     * This method uses a bit mask to match the function field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a function value
     * @return Returns the function field extracted from the reference MIPS instruction
     */
    private int getFunc(int hexInstruction){

        int func = (hexInstruction & maskFunc);

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskFunc);
        Console.debugf("FUNC: 0x%08X", func);
        Console.debugf("","\n");

        return func;
    }



    /**
     * This method uses a bit mask to match the offset field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a offset value
     * @return Returns the offset field extracted from the reference MIPS instruction
     */
    private short getOffset(int hexInstruction){

        // ** SIGNED SHORT **

        short offset = (short) (hexInstruction & maskOffset);

        Console.debugf("INSTRUCTION: 0x%08X", hexInstruction);
        Console.debugf("MASK: 0x%08X", maskOffset);
        Console.debugf("OFFSET: 0x%08X", offset);
        Console.debugf("","\n");

        return offset;
    }


    private String getFuncWord(int func){

        /*
         *  add = Base10/Decimal 32
         *  sub = Base10/Decimal 34
         *  and = Base10/Decimal 36
         *  or  = Base10/Decimal 37
         *  slt = Base10/Decimal 42
         */
        if(func == 32)
            return "add";

        if(func == 34)
            return "sub";

        if(func == 36)
            return "and";

        if(func == 37)
            return "or";

        if(func == 42)
            return "slt";

        return null;
    }



}
    


