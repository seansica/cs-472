public class Constants {

    public static final int[] mipsInstructions = {0x032BA020, 0x8CE90014, 0x12A90003, 0x022DA822, 0xADB30020, 0x02697824, 0xAE8FFFF4, 0x018C6020, 0x02A4A825, 0x158FFFF7, 0x8ECDFFF0};

    public static final boolean SYSLOG_DEBUG = false;

    public static final boolean SYSLOG_ERROR = false;

    public static final String LOGGING_FILEPATH = "/Users/seansica/Documents/Boston University/Spring 2020/CS-472 Computer Architecture/Projects/MIPS_Disassembler/src/log.txt";
}
