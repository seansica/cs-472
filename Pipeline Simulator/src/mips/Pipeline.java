package mips;

import mips.stages.*;

public class Pipeline {

    // MIPS Stages
    private final FetchStage IF;    // = new FetchStage();
    private final DecodeStage ID;   // = new DecodeStage();
    private final ExecuteStage EX;  // = new ExecuteStage();
    private final MemStage MEM;     // = new MemStage();
    private final WriteBackStage WB;// = new WriteBackStage();

    // Main Memory
    public static MainMemory Main_Mem = new MainMemory();

    // Registers
    public static Registers Regs = new Registers();

    // Cycle counter
    private static int incrPC = 0;


    // Pipeline Default Constructor
    public Pipeline(int[] instructionCache){

        IF = new FetchStage();
        ID = new DecodeStage();
        EX = new ExecuteStage();
        MEM = new MemStage();
        WB = new WriteBackStage();

        init(instructionCache);
    }


    // Start pipeline workflow
    private void init(int[] instructionCache){

        for (int i = 0; i < instructionCache.length; i++ ) {

            IF_Stage(instructionCache[i]);
            ID_Stage();
            EX_Stage();
            MEM_Stage();
            WB_Stage();
            Print_out_everything();
            Copy_write_to_read();
        }
        //Main_Mem.display();  // UNCOMMENT TO PRINT OUT MAIN MEMORY
    }


    private void Print_out_everything(){

        System.out.printf("**************************** [CLOCK CYCLE %02d] ****************************\n\n", (++incrPC));
        Regs.printRegisters();
        IF.print();
        ID.print();
        EX.print();
        MEM.print();
        WB.print();
    }

    private void Copy_write_to_read(){

        ID.read = new Instruction(IF.write);
        EX.read = new Instruction(ID.write);
        MEM.read = new Instruction(EX.write);
        WB.read = new Instruction(MEM.write);
    }

    private void IF_Stage(int nextAddress){

        IF.execute(nextAddress);
    }

    private void ID_Stage(){

        ID.execute();
    }

    private void EX_Stage(){

        EX.execute();
    }

    private void MEM_Stage(){

        MEM.execute();
    }

    private void WB_Stage(){

        WB.execute();
    }


}
