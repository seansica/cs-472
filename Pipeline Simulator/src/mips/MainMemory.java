package mips;

import mips.stages.Instruction;

public class MainMemory extends Instruction {

    public static final int MAX_MEM_SIZE = 2048;
    private static final int MAX_BYTE_VALUE = 0xFF;   // Refers to Max byte size
    private static final int NUM_SETS = 0x8;          // Refers to number of repeat blocks, e.g. 0x8 (8) * 0xFF (256) = 2048
    private int[] memory;                             // Container for Blocks



    /**
     * Memory Default Constructor
     */
    public MainMemory(){

        // INSTANTIATE MAIN MEMORY
        memory = new int[MAX_MEM_SIZE];

        int blockCounter = 0;
        int data = 0;

        for (int i = 0; i < memory.length; i++) {

            if(data == MAX_BYTE_VALUE + 0x01){
                data = 0;
            }
            memory[i] = data;
            blockCounter++;
            data++;
        }
    }


    /**
     * Method to IF_stage data at specific memory location
     * @param index Refers to the location at which data should be fetched in memory
     * @return Return the integer located at the specified index
     * @throws ArrayIndexOutOfBoundsException Error Handler for indices which do not exist in memory
     */
    public int getBlock(int index) throws ArrayIndexOutOfBoundsException{

        int block = -1;

        try{
            if(index > this.memory.length)
                throw new ArrayIndexOutOfBoundsException("No data exists at index" + index + ". Please try again.");
            else
                block = this.memory[index];
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println();
        }

        //System.out.println("getBlock > return " + block);
        return block;
    }


    /**
     * Prints the contents of memory to the console
     */
    public void display(){

        StringBuilder mem = new StringBuilder();
        int ctr = 0;

        for (int i = 0; i < MAX_MEM_SIZE; i++) {

            if(ctr == 16) {
                mem.append("\n");  // Add newline every 16 blocks
                ctr = 0;
            }
            mem.append(String.format("%02X", memory[i])).append(" ");
            ctr++;
        }

        System.out.println("<START DISPLAY MAIN MEM>");
        System.out.println(mem);
        System.out.println("</END DISPLAY MAIN MEM>");
    }


    /**
     * Fetches the size of memory
     * @return Return the integer size of memory
     */
    public int size() {
        return this.memory.length;
    }


    /**
     * Function to change integer data values at a specified sub-location in memory
     * @param startIndex Refers to the starting point at which data should begin writing to memory
     * @param endIndex Refers to the stopping point after which data should stop writing to memory
     * @param data Refers to the data that should be written to memory
     */
    public void write(int startIndex, int endIndex, int[] data) {

        System.out.println("Memory > write > start at index " + Integer.toHexString(startIndex));
        System.out.println("Memory > write > end at index " + Integer.toHexString(endIndex));

        int j = 0;
        for (int i = startIndex; i <= endIndex; i++) {

            System.out.println("Memory > write > overwriting " + Integer.toHexString(memory[i]) + " with " + Integer.toHexString(data[j]));
            memory[i] = data[j];
            j++;
        }
    }

    public void setBlock(int index, int data) {
        memory[index] = data;
    }
}
