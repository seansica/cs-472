package mips;

public class Registers {

    private static final byte REG_FILE_SIZE = 32;
    private static int[] Regs = new int[REG_FILE_SIZE];


    public Registers(){

        // INITIALIZE REGISTERS
        Regs[0] = 0;
        for(int i = 1; i < REG_FILE_SIZE; i++ ){
            Regs[i] = i + 0x100;
        }

    }


    public int getRegister(int index){
        return Regs[index];
    }


    public void setRegister(int index, int data){
        Regs[index] = data;
    }


    public void printRegisters(){

        System.out.print( "Regs:\n" );
        for( int i = 0; i < Regs.length; i++ ){

            if(i % 8 == 0 && i != 0){

                System.out.println();
            }

            System.out.printf( "  $%2d = 0x%03X, ", i, Regs[i] );
        }
        System.out.println("\n");
    }

    public int size() {
        return Regs.length;
    }
}
