package mips.stages;


public class ExecuteStage{

    public Instruction read, // input buffer to use in EX calculations
                     write; // output buffer to contain EX-calculated values

    public ExecuteStage(){
        write = read = new Instruction(0);

        write.ALU_Result = 0;
    }


    public void execute(){
        write = new Instruction(read);

        // ******* BEGIN CALCULATE ALU RESULT *******************
        //R-Format
        if(read.ALUOpBits == 0b10){

            // If the instruction is R-format then ALUOp will be 10 and that will tell the ALU to look at the function code

            //if(read.funcWord.equals("add"))
            if(read.func == 0x20){
                // ALU uses func code "add"
                write.ALU_Result = (read.ReadReg1 + read.ReadReg2);
            }

            //else if(read.funcWord.equals("sub"))
            else if(read.func == 0x22){
                // ALU uses func code "sub"
                write.ALU_Result = (read.ReadReg1 - read.ReadReg2);
            }
        }
        //I-Format or NOP
        else if(read.ALUOpBits == 0b00){

            // ALU "sb"
            if( !(read.MemRead) && read.MemWrite ){
                //System.out.println("ex > sb hit! " + read.mipsInstruction);
                write.ALU_Result = (read.ReadReg1 + read.offset);
            }
            // ALU "lb"
            if( read.MemRead && !(read.MemWrite) ){
                //System.out.println("ex > lb hit! " + read.mipsInstruction);
                write.ALU_Result = (read.ReadReg1 + read.offset);
            }
            // NOP
            else{
                write.ALU_Result = 0;
            }
        }
        // ******* END CALCULATE ALU RESULT *******************

    }


    public void print() {

        /* **********************************************************************************************************************************
         * ID/EX Read (read by the EX stage)
         * **********************************************************************************************************************************/
        System.out.println("ID/EX Read (read by the EX stage)");
        if(read.opCode == 0)
            System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", read.hexInstruction, read.funcWord);

        if( read.opCode == 0x20)
            System.out.printf("    INSTRUCTION:0x%08X, [lb]\n", read.hexInstruction);

        if( read.opCode == 0x28)
            System.out.printf("    INSTRUCTION:0x%08X, [sb]\n", read.hexInstruction);

        System.out.printf("    CONTROL={RegDst:%b, ALUSrc:%b, ALUOpBits=%02d, MemRead:%b, MemWrite:%b, Branch:%b, MemToReg:%b, RegWrite:%b}\n", read.RegDst, read.ALUSrc, Integer.parseInt(Integer.toBinaryString(read.ALUOpBits)), read.MemRead, read.MemWrite, read.Branch, read.MemToReg, read.RegWrite);

        System.out.printf("    REG_VALS={Reg1Value:%X, Reg2Value:%X, SEOffset:%X, WriteReg:%X, Func:0x%X}\n\n", read.ReadReg1, read.ReadReg2, read.offset, read.WriteReg, read.func);


        /* **********************************************************************************************************************************
         * EX/MEM Write (written to by the EX stage)
         * **********************************************************************************************************************************/
        System.out.println("EX/MEM Write (written to by the EX stage)");

        if(read.opCode == 0)
            System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", write.hexInstruction, write.funcWord);

        if( read.opCode == 0x20)
            System.out.printf("    INSTRUCTION:0x%08X, [lb]\n", write.hexInstruction);

        if( read.opCode == 0x28)
            System.out.printf("    INSTRUCTION:0x%08X, [sb]\n", write.hexInstruction);

        System.out.printf("    CONTROL={MemRead:%b, MemWrite:%b, Branch:%b, MemToReg:%b, RegWrite:%b}\n", write.MemRead, write.MemWrite, write.Branch, write.MemToReg, write.RegWrite);

        System.out.printf("    DATA={SB_Value:0x%X, WriteRegNum:%d, Reg2Value:0x%X, ALU_Result:0x%X}\n\n", write.WriteData, write.WriteReg, write.ReadReg2, write.ALU_Result);

    }



}
