package mips.stages;


public class Instruction {

    int hexInstruction; // raw instruction field

    // instruction fields - id
    int        opCode,  // i+r format, opcode field,   bits 26-31
                 src1,  // i+r-format, src field,      bits 21-25
              src2Dst,  // i+r-format, src2dst field,  bits 16-20
                  dst,  // r-format,   dst field,      bits 11-15
                xBits,  // i-format,   x-bits field,   bits 6-10
                 func;  // r-format,   func field,     bits 0-5
    int        offset; // i-format, offset field (bits 15-0)
    String opCodeWord, // english mips translation of op-code
             funcWord, // english mips translation of function field
      mipsInstruction; // human-readable mips instruction, i.e. add $3 $1 $2

    // control bits
    int   ALUOpBits;   // ex control bit, write, 2 bit ((10=R-Format -> use func) || (00=I-Format -> func=add) || (01=Branch -> func=sub))
    boolean  RegDst,   // ex control bit, write
             ALUSrc,   // ex control bit, write
             Branch,   // mem control bit, write
            MemRead,   // mem control bit, write
           MemWrite,   // mem control bit, write
           RegWrite,   // wb control bit, write
           MemToReg;   // wb control bit, write

    // set registers - id/ex
    int    ReadReg1,   // ex out reg value, write
           ReadReg2,   // ex out reg value, write
           WriteReg;   // ex out reg value, write

    // ex write values
    int ALU_Result,
         WriteData;


    /**
     * Default constructor for stage variable accessors and mutators
     * @param hexInstruction Init mem address from which to derive instruction fields
     */
    public Instruction(int hexInstruction){


        this.hexInstruction = hexInstruction;
        opCode = 0;
        src1 = 0;
        src2Dst = 0;
        dst = 0;
        xBits = 0;
        func = 0;
        offset = 0;
        opCodeWord = "";
        funcWord = "";
        mipsInstruction = "";

    }

    /**
     * Zero constructor
     */
    protected Instruction(){
        this(0);
    }

    public Instruction(Instruction other){
        this.hexInstruction = other.hexInstruction;
        RegDst = other.RegDst;
        ALUOpBits = other.ALUOpBits;
        ALUSrc = other.ALUSrc;
        MemRead = other.MemRead;
        MemWrite = other.MemWrite;
        RegWrite = other.RegWrite;
        MemToReg = other.MemToReg;
        Branch = other.Branch;
        ReadReg1 = other.ReadReg1;
        ReadReg2 = other.ReadReg2;
        WriteReg = other.WriteReg;
        ALU_Result = other.ALU_Result;
        WriteData = other.WriteData;
        opCode = other.opCode;
        src1 = other.src1;
        src2Dst = other.src2Dst;
        dst = other.dst;
        xBits = other.xBits;
        func = other.func;
        offset = other.offset;
        opCodeWord = other.opCodeWord;
        funcWord = other.funcWord;
        mipsInstruction = other.mipsInstruction;
    }


    /* *********************************
     * Accessor and mutator methods
     * *********************************
     */

    public int ALUOp() {
        return ALUOpBits;
    }

    public boolean RegDst() {
        return RegDst;
    }

    public boolean ALUSrc() {
        return ALUSrc;
    }

    public boolean Branch() {
        return Branch;
    }

    public boolean MemRead() {
        return MemRead;
    }

    public boolean MemWrite() {
        return MemWrite;
    }

    public boolean RegWrite() {
        return RegWrite;
    }

    public boolean MemToReg() {
        return MemToReg;
    }

    public int ReadReg1() {
        return ReadReg1;
    }

    public int getReadReg2() {
        return ReadReg2;
    }

    public int getWriteReg() {
        return WriteReg;
    }

    public int getALUResult() {
        return ALU_Result;
    }

    public int getWriteData() {
        return WriteData;
    }

    public int getAddress(){
        return this.hexInstruction;
    }

    public void setAddress(int address){
        this.hexInstruction = address;
    }

    public String getFuncWord() {
        return this.funcWord;
    }

    public int getDst(){
        return this.dst;
    }

    public int getSrc1(){
        return this.src1;
    }

    public int getSrc2Dst(){
        return this.src2Dst;
    }

    public int getFunc(){
        return this.func;
    }

    public int getOpCode(){
        return opCode;
    }

    public String getOpCodeWord(){
        return opCodeWord;
    }

    public int getxBits(){
        return this.xBits;
    }

    public int getOffset() {
        return this.offset;
    }

    public String getMipsFields(){
        return this.mipsInstruction;
    }

    public void setMipsFormat(){

        if(opCode == 0) {
            mipsInstruction = funcWord + " $" + dst + " $" + src1 + " $" + src2Dst;
        }
        else{
            mipsInstruction = opCodeWord + " $" + src2Dst + ", " + offset + "($" + src1 + ")";
        }
    }

    public String printRFormatFields(){

        if(opCode != 0) {
            return "I-Format Instruction Detected in R-Format Disassembler";
        }
        // Print R-Format Instruction
        return "[" + funcWord + " $" + dst + " $" + src1 + " $" + src2Dst + "]";
    }

    public String printIFormatFields(){

        // Error handling
        if (opCode == 0){
            return "R-Format Instruction Detected in I-Format Disassembler";
        }
        // Print I-Format Instruction
        return "[" + opCodeWord + " $" + src2Dst + ", " + offset + "($" + src1 + ")" + "]";
    }

    public Instruction clear() {
        return new Instruction(0);
    }

}
