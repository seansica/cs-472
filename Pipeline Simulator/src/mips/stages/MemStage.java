package mips.stages;

import mips.Pipeline;

public class MemStage{



    public Instruction read, // input buffer to use in MEM calculations
                     write; // output buffer to contain MEM-calculated values


    public MemStage(){
        read = write = new Instruction(0);

        write.RegWrite = false;
        write.MemToReg = false;
        write.MemWrite =false;
        write.MemRead = false;
    }


    public void execute(){

        write = new Instruction(read);

        // If lb, then index into Main_Mem[ALU_Result] and get the value

        // lb
        if(read.MemRead && !read.MemWrite){

            write.WriteData = Pipeline.Main_Mem.getBlock( read.ALU_Result ); // main[index]
        }

        // sb
        if(!read.MemRead && read.MemWrite){

            Pipeline.Main_Mem.setBlock( read.ALU_Result, read.WriteData );
        }
    }


    public void print() {

        /* **********************************************************************************************************************************
         * EX/MEM Read (read by the MEM stage)
         * **********************************************************************************************************************************/
        System.out.println("EX/MEM Read (read by the MEM stage)");

        if(read.opCode == 0)
            System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", read.hexInstruction, read.funcWord);

        if( read.opCode == 0x20)
            System.out.printf("    INSTRUCTION:0x%08X, [lb]\n", read.hexInstruction);

        if( read.opCode == 0x28)
            System.out.printf("    INSTRUCTION:0x%08X, [sb]\n", read.hexInstruction);

        System.out.printf("    CONTROL={MemRead:%b, MemWrite:%b, Branch:%b, MemToReg:%b, RegWrite:%b}\n", read.MemRead, read.MemWrite, read.Branch, read.MemToReg, read.RegWrite);

        System.out.printf("    DATA={SB_Value:0x%X, WriteRegNum:%d, Reg2Value:0x%X, ALU_Result:0x%X}\n\n", read.ReadReg2, read.WriteReg, read.WriteData, read.ALU_Result);



        /* **********************************************************************************************************************************
         * MEM/WB Write (written to by the MEM stage)
         * **********************************************************************************************************************************/
        System.out.println("MEM/WB Write (read by the MEM stage)");

        if(read.opCode == 0)
            System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", write.hexInstruction, write.funcWord);

        if( read.opCode == 0x20)
            System.out.printf("    INSTRUCTION:0x%08X, [lb]\n", write.hexInstruction);

        if( read.opCode == 0x28)
            System.out.printf("    INSTRUCTION:0x%08X, [sb]\n", write.hexInstruction);

        System.out.printf("    CONTROL={MemToReg:%b, RegWrite:%b}\n", write.MemToReg, write.RegWrite);

        System.out.printf("    DATA={WriteRegNum:%d, LB_WriteData:0x%X, ALU_Result:0x%X}\n\n", write.WriteReg, write.WriteData, write.ALU_Result);
    }


}
