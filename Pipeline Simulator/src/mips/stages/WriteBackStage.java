package mips.stages;

import mips.Pipeline;

public class WriteBackStage{


    public Instruction read, // input buffer to use in WB calculations
                     write; // output buffer to contain WB-calculated values


    // R-Type and Load Operands
    public WriteBackStage(){
        read = write = new Instruction(0);

        write.MemToReg = false;
        write.MemWrite = false;
    }

    public void execute(){
        // ALU Result determines the address in Main_Mem where WriteData is currently stored
        // then WriteReg tells us where to store WriteData
        write = new Instruction(read);

        // I-Format (Load Operand)
        if(read.RegWrite){

            if (read.MemToReg) {
                Pipeline.Regs.setRegister(read.WriteReg, read.WriteData);
            }
            // IF R-Format
            else{
                Pipeline.Regs.setRegister(read.WriteReg, read.ALU_Result);
            }
        }
    }

    public void print(){

        /* **********************************************************************************************************************************
         * MEM/WB READ (read by the WB stage)
         * **********************************************************************************************************************************/
        System.out.println("MEM/WB Read (read by the WB stage)");

        if(read.opCode == 0)
            System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", read.hexInstruction, read.funcWord);

        if( read.opCode == 0x20)
            System.out.printf("    INSTRUCTION:0x%08X, [lb]\n", read.hexInstruction);

        if( read.opCode == 0x28)
            System.out.printf("    INSTRUCTION:0x%08X, [sb]\n", read.hexInstruction);

        System.out.printf("    CONTROL={MemToReg:%b, RegWrite:%b}\n", read.MemToReg, read.RegWrite);

        System.out.printf("    DATA={WriteRegNum:%d, LB_WriteData:0x%X, ALU_Result:0x%X}\n\n", read.WriteReg, read.WriteData, read.ALU_Result);

        if(read.MemToReg)
            System.out.printf("    $%d is set to a new value of 0x%X\n\n", read.WriteReg, Pipeline.Regs.getRegister(read.WriteReg ));
    }
}
