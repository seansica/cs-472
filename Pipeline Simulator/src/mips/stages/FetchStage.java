package mips.stages;

public class FetchStage{


    public Instruction write;


    public FetchStage(){

        write = new Instruction(0);

    }

    public void execute(int address){

        write = new Instruction(address);
    }


    public void print(){

        System.out.println("IF/ID_Write (written to by the IF stage)");
        System.out.printf("    INSTRUCTION:0x%08X\n\n", write.hexInstruction);
    }

}
