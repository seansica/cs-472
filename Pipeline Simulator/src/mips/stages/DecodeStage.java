package mips.stages;

import mips.Pipeline;


public class DecodeStage{

    public Instruction read, // input buffer from IF stage to use in ID calculations
                     write; // output buffer to contain ID-calculated values

    public DecodeStage(){
        read = write = new Instruction(0);

        write.RegDst = false;
        write.ALUSrc = false;
        write.MemRead = false;
        write.MemWrite = false;
        write.MemToReg = false;
        write.RegWrite = false;
        write.Branch = false;
        write.ReadReg1 = 0;
        write.ReadReg2 = 0;
        write.WriteReg = 0;
        write.ALUOpBits = 0b00;
    }

    /**
     * This method uses a bitwise mask to match and return the Op Code field from a MIPS instruction
     * @param hexInstruction This is the 32-bit MIPS instruction that will be referenced
     * @return A 32-bit integer containing only the matched Op Code bits; all non-matching bits are set to zero
     */
    public void execute(){

        //write.copyFrom(read);
        write = new Instruction(read);

        int hexInstruction = read.hexInstruction;

        // First, calculate op code to determine instruction format
        int opCode = getOpCode(hexInstruction);

        write.opCode = opCode;

        if( opCode == 0x20)
            write.opCodeWord = "lb"; // used for console print out of mips instruction; not used in computation

        if( opCode == 0x28)
            write.opCodeWord = "sb"; // used for console print out of mips instruction; not used in computation

        // R-FORMAT
        if(opCode == 0)
            disassembleRFormatFields(hexInstruction);

        // I-FORMAT
        else
            disassembleIFormatFields(hexInstruction);

        /********** START DEBUG TESTING ***************************************************************************/
//        System.out.printf("decode > read > instruction=0x%08X\n", read.address);
//        System.out.printf("decode > read > disassembly=%s\n", this.write.mipsInstruction);
//        System.out.printf("decode > read > registers={ %d, %d, %d }\n", this.read.ReadReg1, this.read.ReadReg2, this.read.WriteReg);
//        System.out.printf("decode > read > control bits={ RegDst=%b, ALUSrc=%b, ALUOp=%b, MemRead=%b, MemWrite=%b, Branch=%b, MemToReg=%b, RegWrite=%b\n",
//                this.read.RegDst, this.read.ALUSrc, this.read.ALUOp, this.read.MemRead,
//                this.read.MemWrite, this.read.Branch, this.read.MemToReg, this.read.RegWrite);
//        System.out.println();
        /********** END DEBUG TESTING ***************************************************************************/

        write.setMipsFormat();
        setControlBits();
        setRegisters();

        /********** START DEBUG TESTING ***************************************************************************/
//        System.out.printf("decode > write > address=0x%08X\n", write.address);
//        System.out.printf("decode > write > instruction=%s\n", write.mipsInstruction);
//        System.out.printf("decode > write > fields={opCode:0x%X(%s), src1:%d, src2dst:%d, dst:%d, offset:0x%X, func:0x%X(%s)}\n", write.opCode, write.opCodeWord, write.src1, write.src2Dst, write.dst, write.offset, write.func, write.funcWord);
//        System.out.printf("decode > write > controlbits={RegDst=%b, ALUSrc=%b, ALUOpBits=0x%02X, MemRead=%b, MemWrite=%b, Branch=%b, MemToReg=%b, RegWrite=%b}\n",
//                write.RegDst, write.ALUSrc, write.ALUOpBits, write.MemRead,
//                write.MemWrite, write.Branch, write.MemToReg, write.RegWrite);
//        System.out.printf("decode > write > registers={ReadReg1:0x%02X, ReadReg2:0x%02X, WriteReg:0x%02X}\n\n", write.ReadReg1, write.ReadReg2, write.WriteReg);
        /********** END DEBUG TESTING ***************************************************************************/

    }



    public void print() {

        /* **********************************************************************************************************************************
         * IF/ID Read (read by the ID stage)
         * **********************************************************************************************************************************/
        System.out.println("IF/ID Read (read by the ID stage)");
        System.out.printf("    INSTRUCTION:0x%08X\n\n", read.hexInstruction);


        /* **********************************************************************************************************************************
         * ID/EX Write (written to by ID stage)
         * **********************************************************************************************************************************/
        System.out.println("ID/EX Write (written to by the ID stage)");
        System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", write.hexInstruction, write.mipsInstruction);
        System.out.printf("    CONTROL_BITS={RegDst:%b, ALUSrc:%b, ALUOpBits=%02d, MemRead:%b, MemWrite:%b, Branch:%b, MemToReg:%b, RegWrite:%b}\n", write.RegDst, write.ALUSrc, Integer.parseInt(Integer.toBinaryString(write.ALUOpBits)), write.MemRead, write.MemWrite, write.Branch, write.MemToReg, write.RegWrite);
        System.out.printf("    REG_VALS={Reg1Value:0x%X, Reg2Value:0x%X, SEOffset:0x%X, WriteRegNum:%d, Func:0x%X}\n\n", write.ReadReg1, write.ReadReg2, write.offset, write.WriteReg, write.func);

        //System.out.printf("    INSTRUCTION:0x%08X, [%s]\n", write.hexInstruction, write.mipsInstruction);
        //System.out.printf("    TEST ReadReg1:%d\n", write.ReadReg1);
        //System.out.printf("    TEST ReadReg2:%d\n", write.ReadReg2);
        //System.out.printf("    TEST WriteReg:%d\n", write.WriteReg);
        //System.out.printf("    TEST Offset:%X\n", write.offset);
        //System.out.printf("    TEST Func:%X [%s]\n", write.ReadReg1, write.funcWord);
    }



    private void setRegisters(){
        /*
         * READ REGISTER 1 = SRC1 field
         * READ REGISTER 2 = SRC/DST field
         * WRITE REGISTER = { RegDst == 1 ? dst : src/dst }
         */

        // R-Format
        if(write.opCode == 0){
            write.ReadReg1 = Pipeline.Regs.getRegister(write.src1);
            write.ReadReg2 = Pipeline.Regs.getRegister(write.src2Dst);
            write.WriteReg = write.dst;  // WriteReg_15_11
        }
        // I-Format
        else{
            write.ReadReg1 = Pipeline.Regs.getRegister(write.src1);
            write.ReadReg2 = 0x0;
            write.WriteReg = write.src2Dst; // WriteReg_20_16
        }

    }



    private void setControlBits() {

        // R-Format (ADD, SUB)
        if(write.opCode == 0){

            //if(write.funcWord.equals("add") || write.funcWord.equals("sub"))
            if(write.func == 0x20 || write.func == 0x22){

                write.RegDst = true;    // valid
                write.ALUSrc = false;   // valid
                write.MemRead = false;  // valid
                write.MemWrite = false; // valid
                write.MemToReg = false; // valid
                write.RegWrite = true;  // valid
                write.Branch = false;   // valid
                write.ALUOpBits = 0b10; // If the instruction is R-format then ALUOp will be 10 and that will tell the ALU to look at the function code

            }
        }

        // I-Format (LB) (Load Byte)

        //if(write.opCodeWord.equals("lb"))
        else if(write.opCode == 0x20){

            write.RegDst = false;   // valid
            write.ALUSrc = true;    // valid
            write.MemRead = true;   // valid
            write.MemWrite = false; // valid
            write.MemToReg = true;  // valid
            write.RegWrite = true;  // valid
            write.Branch = false;   // valid
            write.ALUOpBits = 0b00; // Op Code for I-Format = 0x0
        }


        // I-Format (SB) (Store Byte)
        //if(write.opCodeWord.equals("sb"))
        else if(write.opCode == 0x28) {

            write.RegDst = false;   // value does not matter, default to false, validated
            write.ALUSrc = true;    // validated
            write.MemRead = false;  // validated
            write.MemWrite = true;  // validated
            write.MemToReg = false; // don't care, default to false, validated
            write.RegWrite = false; // validated
            write.Branch = false;   // validated
            write.ALUOpBits = 0b00; // Op Code for I-Format = 0x0
        }


        // NOP (No Operation)
        else{

            write.RegDst = false;   // don't care, default to false
            write.ALUSrc = false;   // don't care, default to false
            write.MemRead = false;  // don't care, default to false
            write.MemWrite = false; // don't care, default to false
            write.MemToReg = false; // don't care, default to false
            write.RegWrite = false; // don't care, default to false
            write.Branch = false;   // don't care, default to false
            write.ALUOpBits = 0b00; // Op Code for I-Format = 0x0
        }

    }


    /**
     * This method builds an integer array of all R-Formatted fields for MIPS instructions
     * @param mipsInstruction This is the 32-bit integer that will be analyzed
     */
    private void disassembleRFormatFields(int mipsInstruction){

        // Error handling
        if(write.opCode != 0) {
            return;
        }
        // Calculate R-Format fields
        write.src1 = getSrc1(mipsInstruction);
        write.src2Dst = getSrc2Dst(mipsInstruction);
        write.dst = getDst(mipsInstruction);
        write.xBits = getXBits(mipsInstruction);
        write.func = getFunc(mipsInstruction);;
        write.funcWord = getFuncWord(write.func);
        //write.offset = getOffset(mipsInstruction);
    }

    /**
     * This method builds an integer array of all I-Formatted fields for MIPS instructions
     * @param mipsInstruction This is the 32-bit integer that will be analyzed
     * @return An integer array of R-Formatted fields for MIPS instructions
     */
    private void disassembleIFormatFields(int mipsInstruction){

        // Error handling
        if (write.opCode == 0){
            //System.out.println("R-Format Instruction Detected in I-Format Disassembler");
            return;
        }
        // Calculate I-Format specific fields
        write.src1 = getSrc1(mipsInstruction);
        write.src2Dst = getSrc2Dst(mipsInstruction);
        write.offset = getOffset(mipsInstruction);
    }


    /**
     * This method uses a bit mask to match the OP Code field of a MIPS instruction
     * @param hexInstruction This is the reference MIPS instructions of which we will derive a OP Code value
     * @return Returns the OP Code field extracted from the reference MIPS instruction
     */
    private int getOpCode(int hexInstruction){

        // Bit Masks
        int maskOpCode = 0xFC000000;  // 111111-00000-00000-00000-00000-000000
        int opCode = (hexInstruction & maskOpCode) >>> 26;

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskOpCode);
        //System.out.printf("OPCODE: 0x%X \n", opCode);

        return opCode;
    }

    /**
     * This method uses a bit mask to match the src1 field of a MIPS instruction
     * @param hexInstruction This is the reference MIPS instructions of which we will derive a src1 value
     * @return Returns the src1 field extracted from the reference MIPS instruction
     */
    private int getSrc1(int hexInstruction){

        // 000000-11111-00000-00000-00000-000000
        int maskSrc1 = 0x03E00000;
        int src1 = (hexInstruction & maskSrc1) >> 21;

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskSrc1);
        //System.out.printf("SRC1: 0x%X \n", src1);

        return src1;
    }

    /**
     * This method uses a bit mask to match the src2/dst field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a src2/dst value
     * @return Returns the src2/dst field extracted from the reference MIPS instruction
     */
    private int getSrc2Dst(int hexInstruction){

        // 000000-00000-11111-00000-00000-000000
        int maskSrc2Dst = 0x001F0000;
        int src2Dst = (hexInstruction & maskSrc2Dst) >> 16;

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskSrc2Dst);
        //System.out.printf("SRC2DST: 0x%08X \n", src2Dst);

        return src2Dst;
    }

    /**
     * This method uses a bit mask to match the dst field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a dst value
     * @return Returns the dst field extracted from the reference MIPS instruction
     */
    private int getDst(int hexInstruction){

        // 000000-00000-00000-11111-00000-000000
        int maskDst = 0x0000F800;
        int dst = (hexInstruction & maskDst) >> 11;

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskDst);
        //System.out.printf("DST: 0x%08X \n", dst);

        return dst;
    }

    /**
     * This method uses a bit mask to match the X-Bits field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a X-Bits value
     * @return Returns the X-Bits field extracted from the reference MIPS instruction
     */
    private int getXBits(int hexInstruction){

        // 000000-00000-00000-00000-11111-000000
        int maskXBits = 0x000007C0;
        int xBits = (hexInstruction & maskXBits);

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskXBits);
        //System.out.printf("XBITS: 0x%08X \n", xBits);

        return xBits;
    }

    /**
     * This method uses a bit mask to match the function field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a function value
     * @return Returns the function field extracted from the reference MIPS instruction
     */
    private int getFunc(int hexInstruction){

        // 000000-00000-00000-00000-00000-111111
        int maskFunc = 0x0000003F;
        int func = (hexInstruction & maskFunc);

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskFunc);
        //System.out.printf("FUNC: 0x%08X \n", func);

        return func;
    }

    /**
     * This method uses a bit mask to match the offset field of a MIPS instruction
     * @param hexInstruction his is the reference MIPS instructions of which we will derive a offset value
     * @return Returns the offset field extracted from the reference MIPS instruction
     */
    private int getOffset(int hexInstruction){

        // ** SIGNED SHORT **

        // 000000-00000-00000-1111111111111111
        int maskOffset = 0x0000FFFF;
        int offset = (hexInstruction & maskOffset);

        //System.out.printf("INSTRUCTION: 0x%08X \n", hexInstruction);
        //System.out.printf("MASK: 0x%08X \n", maskOffset);
        //System.out.printf("OFFSET: 0x%08X \n", offset);

        return sign_extend(offset, 16);
    }


    public static int sign_extend(int val, int bits) {
        int shift = 32 - bits;  // int always has 32 bits in Java
        int s = val << shift;
        return s >> shift;
    }


    private String getFuncWord(int func){

        /*
         *  add = Base10/Decimal 32
         *  sub = Base10/Decimal 34
         *  and = Base10/Decimal 36
         *  or  = Base10/Decimal 37
         *  slt = Base10/Decimal 42
         */
        if(func == 0)
            return "nop";

        if(func == 0x20)
            return "add";

        if(func == 0x22)
            return "sub";

        return null;
    }



}
