# Pipeline Simulator

This is a Java-based implementation of the MIPS pipeline workflow.

![Pipeline Workflow](Pipeline_Workflow.png)