package Simulation;

import java.util.Arrays;

public class Slot{

    private static final byte MAX_BLOCK_SIZE = 16;
    private boolean dirty;
    private boolean valid;
    private int tag;
    private int slot;
    private int[] data;
    private int address; // used to remember where to write to memory after dirty bit flipped


    public Slot(int slot){
        this.tag = 0;
        this.slot = slot;
        data = new int[MAX_BLOCK_SIZE];
        Arrays.fill(data, 0, MAX_BLOCK_SIZE, (short) 0);
        boolean dirty = false;
        boolean valid =  false;
    }

    // GETTER
    public int getAddress() {
        return address;
    }

    // GETTER
    public boolean isDirty() {
        if(dirty)
            return true;

        return false;
    }

    // GETTER
    public int getDirtyBit(){
        if(dirty)
            return 1;

        return 0;
    }

    // GETTER
    public boolean isValid() {
        if(valid)
            return true;

        return false;
    }

    // GETTER
    public int getValidBit() {
        if(valid)
            return 1;

        return 0;
    }

    // GETTER
    public int getTag(){
        return this.tag;
    }

    // GETTER
    public int getSlot(){
        return this.slot;
    }

    // GETTER
    public int[] getData(){
        return this.data;
    }

    // GETTER
    public int getData(int index){
        //System.out.println("Slot > getData > return index " + index);
        return this.data[index];
    }

    // SETTER
    public void setAddress(int address){
        this.address = address;
    }

    // SETTER
    public void setValid(boolean valid){
        this.valid = valid;
    }

    // SETTER
    public void setDirty(boolean dirty){
        this.dirty = dirty;
    }

    // SETTER
    public void setTag(int tag){
        this.tag = tag;
    }


    // SETTER
    public void setData(int offset, int data){
        this.data[offset] = data;
    }

    // SETTER
    public void setData(int[] data){
        this.data = data;
    }

    // SETTER
    public void setSlot(int slot){
        this.slot = slot;
    }



    public boolean hit(Slot slot){

        // SLOT MATCH
        if(slot.getTag() == this.tag){

            // TAG MATCH
            if(slot.getSlot() == this.slot){

                // VALID BIT MATCH
                if(slot.isValid()){
                    return true;
                }
            }
        }
        return false;
    }


    public String showData(){
        String data = "";
        for(int s: this.data){
            // Print slot_num + offset_data_val
            // e.g. Init slot 1 Data = 10 11 12 13 14 15 16 17 18 ... 1F
            data += (Integer.toHexString(s));
            data += " ";
        }
        return data;
    }
}
