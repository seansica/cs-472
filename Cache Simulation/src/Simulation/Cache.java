package Simulation;

import java.util.ArrayList;


public class Cache{

    private static final int MAX_VALUE = 0xF;   // Refers to max size of cache, e.g. 16 slots
    private ArrayList<Slot> cache;              // Container for Slots


    /**
     * Cache Default Constructor
     */
    public Cache(){

        cache = new ArrayList<>();     // Instantiate cache

        for (int i = 0; i < MAX_VALUE + 1; i++) {
            Slot s = new Slot(i);
            cache.add(s);     // Populate memory in range 0,2,3...,F to all zeros
        }
    }


    /**
     * Method to provide read functionality to the Cache based on Tag, Slot, Offset, and other parameters
     * @param memory Refers to the Memory object working in conjunction with Cache
     * @param address Refers to the specific address that should be read in the Cache
     * @param data Refers to the data to be written as part of the Cache write operation
     * @return True if write operation is successful (it always is)
     */
    public boolean write(Memory memory, int address, int data){

        /*
         * e.g. What address would you like to write to? > 562
         * What data would you like to write at that address? > 2f
         * Value 2f has been written to address 562. (Cache Hit)
         *
         * 1. Prompt what address user want to write to + what data user wants to write
         * 2. Extract Tag, Slot, Offset
         * 3. Search cache for matching Slot + Tag
         * 4. If found, check Valid Bit
         *    If Valid, check Dirty Bit
         *      If Dirty
         *          Reconcile() > Copy matching Slot.data to
         *      If Clean
         *          We have a match! Write data to Slot + Set Dirty Bit.
         * If matched Slot has Valid Bit Set, continue
         *      Else go to Memory Block that corresponds to
         */

        // DERIVE TSO DATA FROM ADDRESS
        int[] addrInfo = processAddress(address);
        int tag = addrInfo[0];
        int slot = addrInfo[1];
        int offset = addrInfo[2];

        System.out.println("\nW");
        System.out.println("ADDR: " + Integer.toHexString(address));
        System.out.println("DATA: " + Integer.toHexString(data));
        System.out.println("Tag: " + tag);
        System.out.println("Slot: " + Integer.toHexString(slot));
        System.out.println("Offset: " + Integer.toHexString(offset));


        /*
         * TAG TRUE
         *   VALID TRUE
         *     hit: sw + set dirty
         *   VALID FALSE
         *     miss: lw, write, setDirty
         * TAG FALSE
         *   miss: lw, write, setDirty
         *
         */

        Slot s = cache.get(slot);

        // TAG TRUE
        if(((s.getTag()) == tag)){

            // VALID TRUE
            if(s.isValid()){

                // CACHE HIT
                cache.get(slot).setData(offset, data);
                cache.get(slot).setDirty(true);
                cache.get(slot).setAddress(address); // Store address for later; need to remember where to store in memory if read operation occurs on this Slot after dirty bit flipped
                System.out.printf( "Value %X has been written to address %X (Cache Hit)\n", cache.get(slot).getData(offset), address );
                //display();
                return true;
            }
            // VALID FALSE
            else{
                writeMiss(memory, address, slot, offset, data);
                return true;
            }
        }
        // TAG FALSE
        else{
            writeMiss(memory, address, slot, offset, data);
            return true;
        }
    }


    /**
     * Method to handle write operations which result in a cache-miss
     * @param memory Refers to the Memory object working in conjunction with Cache
     * @param address Refers to the specific address that should be read in the Cache
     * @param slot Refers to the Slot field of the Cache write operation
     * @param offset Refers to the Slot field of the Cache write operation
     * @param data Refers to the data to be written as part of the Cache write operation
     */
    private void writeMiss(Memory memory, int address, int slot, int offset, int data){

        loadBlock(memory, address);
        cache.get(slot).setAddress(address); // Store address for later; need to remember where to store in memory if read operation occurs on this Slot after dirty bit flipped
        cache.get(slot).setData(offset, data);
        cache.get(slot).setDirty(true);
        System.out.printf( "Value %X has been written to address %X (Cache Miss)\n", cache.get(slot).getData(offset), address );
        display();
    }


    /**
     * Method to provide read functionality to the Cache based on Tag, Slot, Offset, and other parameters
     * @param memory Refers to the Memory object working in conjunction with Cache
     * @param address Refers to the specific address that should be read in the Cache
     * @return True if read operation is successful (it always is)
     */
    public boolean read(Memory memory, int address){

        // DERIVE TSO DATA FROM ADDRESS
        int[] addrInfo = processAddress(address);
        int tag = addrInfo[0];
        int slot = addrInfo[1];
        int offset = addrInfo[2];

        Slot s = cache.get(slot);

        System.out.println("\nR");
        System.out.println(Integer.toHexString(address));
        System.out.println("Tag: " + tag);
        System.out.println("Slot: " + Integer.toHexString(slot));
        System.out.println("Offset: " + Integer.toHexString(offset));


        /* ***************
         * CHECK TAG
         * ***************
         *
         * tag true:
         *   |--valid true: hit
         *   |--valid false: miss,lw
         * tag false:
         *   |--valid true:
         *        |--dirty true: miss,sw
         *        |--dirty false: miss,lw
         *   |--valid false: miss,lw
         *
         */

        // TAG EQUAL
        if(((s.getTag()) == tag)){

            // VALID BIT FALSE
            if(!s.isValid()){

                // CACHE MISS: LW - LOAD FROM MAIN MEMORY
                loadBlock(memory, address);
                System.out.println("At that byte there is the value " + Integer.toHexString(s.getData(offset)) + " (Cache Miss)");
                //display();
                return true;
            }

            // VALID BIT TRUE
            else{
                System.out.println("At that byte there is the value " + Integer.toHexString(s.getData(offset)) + " (Cache Hit!)");
                //display();
                return true;
            }
        }

        // TAG NOT EQUAL
        else{

            // VALID BIT FALSE
            if(!s.isValid()){

                // CACHE MISS: LW - LOAD FROM MAIN MEMORY
                loadBlock(memory, address);
                System.out.println("At that byte there is the value " + Integer.toHexString(s.getData(offset)) + " (Cache Miss)");
                //display();
                return true;
            }

            // VALID BIT TRUE
            else{

                // DIRTY BIT TRUE
                if(s.isDirty()){

                    // CACHE MISS: SW - STORE TO MAIN MEMORY
                    storeBlock(memory, cache.get(slot));
                    System.out.println("At that byte there is the value " + Integer.toHexString(s.getData(offset)) + " (Cache Miss)");
                    loadBlock(memory, address);
                    return true;

                }
                else{
                    // CACHE MISS: LW - LOAD FROM MAIN MEMORY
                    loadBlock(memory, address);
                    System.out.println("At that byte there is the value " + Integer.toHexString(s.getData(offset)) + " (Cache Miss)");
                    //display();
                    return true;
                }
            }
        }
    }


    /**
     * This method copies data from Cache to Memory
     * @param memory Refers to the destination data structure to which data should be copied
     * @param address Refers to the specific subset of Memory location to which data should be written
     */
    private void storeBlock(Memory memory, Slot s) {


        // DERIVE TSO DATA FROM ADDRESS
        int[] addrInfo = processAddress(s.getAddress());
        //int tag = addrInfo[0];
        //int slot = addrInfo[1];
        int startIndex = addrInfo[3];
        int endIndex = addrInfo[4];

        // SW - COPY DATA FROM CACHE TO MAIN MEMORY
        //int[] data = cache.get(slot).getData();  // Refers to the source Slot to be copied to main memory, Cache > Slot > Data{0,1,2,...,F]


        memory.write(startIndex, endIndex, s.getData());  // Copies main memory data to corresponding Slot offset


        s.setDirty(false);  // Slot in sync with memory, Dirty=0
        s.setValid(false);   // Slot contents is accurate, Valid=1
        //memory.display();

        //loadBlock(memory, address);
    }


    /**
     * This method copies data from Memory to Cache
     * @param memory This is the source memory from which data will be copied
     * @param address The address is used to compute which specific data segment will be copied into Cache
     */
    private void loadBlock(Memory memory, int address){

        // PREPARE TSO DATA FROM MAIN MEMORY
        int[] addrInfo = processAddress(address);
        int tag = addrInfo[0];
        int slot = addrInfo[1];
        int startIndex = addrInfo[3];
        int endIndex = addrInfo[4];

        byte i = 0;  // Counter to iterate through Slot offsets {0,1,2,...15)

        // LW - COPY DATA FROM MAIN MEMORY TO CACHE
        for(int s = startIndex; s <= endIndex; s++){
            int memBlock = memory.getBlock(s);  // Refers to the slice of data copied from main memory, [0,1,2,...,{startMemBlock,...,endMemBlock},..., 048]
            cache.get(slot).setData(i, memBlock);  // Copies main memory data to corresponding Slot offset
            i++;  // Increment Slot counter until last offset xF written to
        }

        cache.get(slot).setTag(tag);         // Set tag based on read address
        cache.get(slot).setDirty(false);     // Slot in sync with memory, Dirty=0
        cache.get(slot).setValid(true);      // Slot contents is accurate, Valid=1
    }


    /**
     * This method is a simple consolidated way to compute tag, slot, offset, and other information derived from a source address
     * @param address This is the reference address from which data will be derived
     * @return Returns a static array filled with the computed data
     */
    private int[] processAddress(int address){

        // PREPARE TSO DATA FROM MAIN MEMORY
        int tag = Util.getTag(address);
        int slot = Util.getSlot(address);
        int offset = Util.getOffset(address);

        // DERIVE START/END INDICES FOR SW OPERATIONS (NEED TO KNOW WHERE TO WRITE IN MAIN MEM ARRAY)
        int startIndex = Util.getBegin(address);
        int endIndex = (startIndex + MAX_VALUE);

        // STORE DATA
        int[] addrInfo = new int[5];
        addrInfo[0] = tag;
        addrInfo[1] = slot;
        addrInfo[2] = offset;
        addrInfo[3] = startIndex;
        addrInfo[4] = endIndex;

        return addrInfo;

    }


    /**
     * Displays the cache, including slot, valid bit, tag, dirty bit, and the entire data/offset array
     */
    public void display(){

        System.out.println("\nD");
        System.out.println("Slot   Valid  Tag  Dirty     Data");
        cache.forEach(slot -> System.out.println(String.format("%02X", slot.getSlot()) + "     " + slot.getValidBit() + "      " + slot.getTag() + "      " + slot.getDirtyBit() + "       " + slot.showData()));
    }


    /**
     * This method fetches a specified Slot
     * @param index Derives the Slot to be returned
     * @return Return the Slot at the Cache's specified data index
     */
    private Slot getSlot(int index) {
        return (Slot) this.cache.toArray()[index];
    }


    /**
     * Gets the size of the Cache
     * @return Size of Cache
     */
    public int size() {
        return this.cache.size();
    }
}
