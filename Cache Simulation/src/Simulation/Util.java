package Simulation;

import java.math.BigInteger;

/**
 * Utility class for computational methods
 */
public final class Util {

    /**
     * Private constructor to prevent instantiation
     */
    private Util(){
        throw new UnsupportedOperationException();
    }


    /**
     * Computes the tag value from a given address
     * @param addr Refers to the reference address from which tag value is derived
     * @return Return integer value representing a tag
     */
    public static int getTag(int addr){
        return ( addr & 0xF00 ) >>> 8;  // 0xF00 = 0B111100000000
    }


    /**
     * Computes the slot value from a given address
     * @param addr Refers to the reference address from which slot value is derived
     * @return Return integer value representing a slot
     */
    public static int getSlot(int addr){
        return ( addr & 0x0F0 ) >>> 4; // 0x0F0 = 0B000011110000
    }


    /**
     * Computes the offset value from a given address
     * @param addr Refers to the reference address from which offset value is derived
     * @return Return integer value representing an offset
     */
    public static int getOffset(int addr){
        return ( addr & 0x00F );  // 0x00F = 0B000000001111
    }


    /**
     * Computes the starting index at which a read or write operation interacts with memory
     * @param addr Refers to the reference address from which index value is derived
     * @return Return integer value representing the starting index
     */
    public static int getBegin(int addr){
        //System.out.println("getBegin > addr > " + addr);
        //System.out.println("getBegin > mask > " + 0xFF0);
        //System.out.println("getBegin > return > " + (addr & 0xFF0));
        return ( addr & 0xFF0 );  // 0xFF0 = 0B111111110000

    }
}
