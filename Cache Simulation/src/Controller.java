import Simulation.*;

public class Controller {

    private static final Scanner inputCommands = new Scanner();



    public static void main(String[] args){

        start(new Memory(), new Cache(), new Scanner());
    }


    /**
     * Method to commence program simulation.
     * @param memory Refers to a simulated Memory instance
     * @param cache Refers to a simulated Cache instance
     * @param scanner Refers to a Scanner object responsible for automatically piping in command input to Controller
     */
    private static void start(Memory memory, Cache cache, Scanner scanner) {

        memory.display();
        //cache.display();
        //System.exit(0);

        boolean programRun = true;

        while(programRun){

            String cmd = inputCommands.getInput().remove();

            switch(cmd){

                case "R" :
                    int rAddress = Integer.parseInt(inputCommands.getInput().remove(), 16);
                    cache.read(memory, rAddress);
                    cache.display();
                    break;

                case "W" :
                    int writeAddress = Integer.parseInt(inputCommands.getInput().remove(), 16);
                    int data = Integer.parseInt(inputCommands.getInput().remove(), 16);
                    cache.write(memory, writeAddress, data);
                    cache.display();
                    break;

                case "D" :
                    //cache.display();
                    break;

                case "!" :
                    programRun = false;
                    break;
            }
        }
    }
}
