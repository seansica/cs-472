import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;


public class Scanner {

    private static final String FILEPATH = "/Users/seansica/Documents/Boston University/Spring 2020/CS-472 Computer Architecture/Projects/cs-472/Cache Simulation/src/input.txt";
    private static Queue<String> input = new LinkedList<>();

    /**
     * Scanner Default Constructor
     * Serves to automatically read input commands (e.g. Read, Write, Display) into console
     */
    public Scanner(){

        try {
            File file = new File(FILEPATH);
            java.util.Scanner sc = new java.util.Scanner(file);

            // For every line in file,
            // treat line as separate input command
            while (sc.hasNextLine()) {

                // Store command in queue, FIFO
                input.add(sc.nextLine());
            }

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Getter method for console commands
     * @return Serves up a queue of commands based on order of appearance in specified input file
     */
    public Queue<String> getInput(){
        return input;
    }

}
